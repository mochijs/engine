define(['./content/asset'], function(Asset) {

    /**
     * A game state defines a set of parameters that embody a certain moment
     * in the game, such as an 'intro', a 'pause', a 'main.'
     *
     * Certain states come native to the game class, but can be replaced.
     */
    class GameState {
        /**
         * A game state is made up of several major function components.
         * Update, Draw, and Init.
         * @param name The name of the state, used to swap between states.
         */
        constructor(name) {
            this.name = name;
            this._bindings = null;
            this.initialized = false;
        }

        update(ms) {}
        draw(ctx) {}
        init(game, states) {}
        enter(game, states, lastState) {}

        /**
         * Sets up a game state, used as an alternative to extending GameState.
         * @param update Run constantly, passed (ms) seconds since last update.
         * @param draw Run when available, passed (ctx) canvas ctx.
         * @param init Run when the game initializes this state (once when first swapped).
         *        passed (game) The game object, (states) The GameStateManager.
         * @param enter Run when this state is swapped to
         *        passed (game) The game object, (states) GameStateManager, (lastState) state before this.
         */
        setup(update, draw, init, enter) {
            if (update)
                this.update = update;
            if (draw)
                this.draw = draw;
            if (init)
                this.init = init;
            if (enter)
                this.enter = enter;
            return this;
        }

        /**
         * Stores the input bindings that are used within this state.
         */
        storeBindings(bindings) {
            this._bindings = bindings;
        }

        loadBindings() {
            return this._bindings;
        }

    }

    /**
     * Manages the game state objects.
     */
    class GameStateManager {
        constructor(eventBus) {
            this.states = {};
            this.events = eventBus;
            // Current is an empty game state until changed, never null to avoid check on loop.
            this.current = new GameState("default").setup(
                GameStateManager._defaultUpdate,
                GameStateManager._defaultDraw,
                GameStateManager._defaultInit,
                GameStateManager._defaultEnter
            );
        }

        /* Static functions for the default game state */
        static _defaultUpdate(ms) { console.log("Default State Update. Please override!"); }
        static _defaultDraw(ctx) { console.log("Default State Draw. Please override!"); }
        static _defaultInit(game, states) { console.log("State started -- override game.init to customize initialization phase"); }
        static _defaultEnter(game, states, lastState) { console.log("State Entered -- override for entrance."); }

        /**
         * Changes the current game state to the selected state.
         * Does not initialized or prepare the state, only begins the swap.
         * @param name The name of the state to swap to.
         */
        swap (name) {
            if (!(name in this.states))
                throw "Cannot swap state - name [" + name + "] has not been added.";
            let old = this.current;
            this.current = this.states[name];
            this.events.broadcast("game_state_swap", {
                prev: old,
                next: this.current
            }, GameStateManager.Name);
        }

        /**
         * Sets the current state as ready to function.
         */
        ready () {
            this.events.broadcast("game_state_ready", this.current, GameStateManager.Name);
        }

        /**
         * Adds a state to this system.
         * @param state The GameState that should be added.
         */
        add (state) {
            if (state.name in this.states)
                throw "Cannot add state - name [" + state.name + "] already exists.";
            this.states[state.name] = state;
            this.events.broadcast("game_state_add", {
                newState: state,
                allStates: this.states
            }, GameStateManager.Name);
        }

        /**
         * Adds the state to the system and then immediately swaps to it.
         * Convenience method used because of how frequently this occurs.
         * @param state The state to add and swap to.
         */
        set (state) {
            this.add(state);
            this.swap(state.name);
        }

        /**
         * Removes the state with the provided name from this system.
         * @param name The name of the state that should be removed.
         */
        remove (name) {
            if (!(name in this.states))
                throw "Cannot remove state - name [" + name + "] does not exist.";
            let old = this.states[name];
            delete this.states[name];
            this.events.broadcast("game_state_remove", {
                old: old
            }, GameStateManager.Name);
        }
    }
    GameStateManager.Name = "gameStateManager";

    /**
     * A special example state that includes a nice intro for the game before the player is put into it.
     * Built to be fairly customizable.
     */
    class IntroState extends GameState {
        /**
         *
         * @param name The name of this intro state.
         * @param keys An array of string keys that when pressed will transition to the next state.
         * @param next The next state name that will be transitioned to.
         */
        constructor(name, keys, next) {
            super(name);
            this.keys = keys;
            this.next = next;
            // defaults
            this.gameName = "Game!";
            this.subText = "Press " + this.keys.join(', ') + " to start.";
            this.fontColor = "#f8f8f8";
            this.font = '48px tahoma';
            this.subFont = '32px tahoma';
            this.background = true;
            this.backgroundColor = "#3a3a3a";
        }

        /**
         * Changes the displayed name of the game. Can be chained.
         * @param name The name to display instead.
         */
        setGameName(name) {
            this.gameName = name;
            return this;
        }

        /**
         * Changes the displayed subtext (which usually denotes how to start the game). Can be chained.
         * @param subText The subtext to change it to.
         */
        setSubText(subText) {
            this.subText = subText;
            return this;
        }

        /**
         * Changes the font of the main game. Can be chained.
         * @param font The font it should be set to.
         */
        setFont(font) {
            this.font = font;
            return this;
        }

        /**
         * Changes the font of the subtext. Can be chained.
         * @param font The font to set it to.
         */
        setSubFont(font) {
            this.subFont = font;
            return this;
        }

        /**
         * Changes the font color of the header and sub text. Can be chained.
         * @param color The color in hex as a string to change to.
         */
        setFontColor(color) {
            this.fontColor = color;
            return this;
        }

        /**
         * Changes whether or not the color background is displayed. Can be chained.
         * @param background True/False whether this background should be drawn.
         */
        setHasBackground(background) {
            this.background = background;
            return this;
        }

        /**
         * Changes the background color of the background. Can be chained.
         * @param color The color in hex as a string to change to.
         */
        setBackgroundColor(color) {
            this.backgroundColor = color;
            return this;
        }

        init (game, states) {
            // Keep reference to the game for width/height.
            this.game = game;
            // Input initialized here to ensure it is bound to the correct state.
            for (let i = 0; i < this.keys.length; i++) {
                this.game.input.onKeyPress(this.keys[i], this._transition.bind(this));
            }
        }

        /**
         * Transitions to the next state.
         * @private
         */
        _transition() {
            this.game.swapState(this.next);
        }

        enter (game, states, lastState) {

        }

        update(ms) {

        }

        /**
         * Draws the intro state. Basic so it could be overridden for a more complex intro state.
         * @param ctx The context that must be invoked for drawing.
         */
        draw(ctx) {
            ctx.save();
            ctx.clearRect(0, 0, this.game.getPixelWidth(), this.game.getPixelHeight());
            if (this.background)
                this.drawBackground(ctx);
            this.drawOverlayText(ctx);
            ctx.restore();
        }

        drawBackground(ctx) {
            ctx.fillStyle = this.backgroundColor;
            ctx.fillRect(0, 0, this.game.getPixelWidth(), this.game.getPixelHeight());
        }

        /**
         * Draws the text suggesting how to start the game. Allows draw to be overridden.
         * @param ctx The context to start the game with.
         */
        drawOverlayText(ctx) {
            ctx.fillStyle = this.fontColor;
            ctx.font = this.font;
            ctx.textAlign = "center";
            ctx.fillText(this.gameName, this.game.getPixelWidth() / 2, this.game.getPixelHeight() * 0.3);
            ctx.font = this.subFont;
            ctx.fillText(this.subText, this.game.getPixelWidth() / 2, this.game.getPixelHeight() / 2);
        }
    }

    /**
     * Another special example state that shows a nice loading screen along with an ad for the mochi engine.
     *
     */
    class LoadState extends GameState {
        /**
         *
         * @param name The name of this game state
         * @param assets An array of assets that must be loaded in order for this state to transition.
         * @param next The name of the next state to load after all assets are loaded.
         */
        constructor(name, assets, next) {
            super(name);
            this.assets = assets;
            this.next = next;
        }

        init (game, states) {
            // Keep reference to the game for width/height and state swaping.
            this.game = game;
        }

        /**
         * Transitions to the next state.
         * @private
         */
        _load() {
            Asset.LoadAllAssets(this.assets).then(function() {
                this.game.swapState(this.next);
            }.bind(this));
        }

        /** Whenever this state is entered, it attempts to load all assets */
        enter (game, states, lastState) {
            this._load();
        }

        update(ms) {}
    }

    /**
     * The pause state is used to pause the game. The pause screen will continue
     * to draw whatever state was running before it. Cannot start on the PauseState.
     */
    class PauseState extends GameState {
        /**
         * Creates a pause state. Functions like a mini-stack.
         * @param name The name of the state
         * @param keys The set of keys that will cause the state to end and return to the previous state.
         */
        constructor(name, keys) {
            super(name);
            this.keys = keys;
            this.lastState = null;
        }

        init (game, states) {
            this.game = game;
            // Input initialized here to ensure it is bound to the correct state.
            for (let i = 0; i < this.keys.length; i++) {
                this.game.input.onKeyPress(this.keys[i], this.unpause.bind(this));
            }
        }

        /** Unpauses, aka exits the state and swaps to whatever the last state was */
        unpause() {
            if (this.lastState == null)
                throw "Cannot unpause into state that does not exist. Do not start on PauseState.";
            this.game.swapState(this.lastState.name);
        }

        /** Invoked to save state information for what is paused */
        pause(lastState) {
            this.lastState = lastState;
        }

        /** Whenever this state is entered, it attempts to load all assets */
        enter (game, states, lastState) {
            this.pause(lastState);
        }

        update(ms) {}

        /** Draws the previous state and our pause information over it */
        draw (ctx) {
            this.lastState.draw(ctx);
            // TODO Draw a screen overlay that says pause.
        }
    }

    /**
     * A state to transition to when the game is over. Displays a score if given.
     */
    class EndState extends GameState {
        /**
         * Creates a new EndState. Will restart the game (swap to restart state) on keypress.
         * @param name The name of the state.
         * @param restartState The name of the state to "restart" to.
         * @param keys Array of keys that will let you restart the game.
         * @param (o) score Optional score to display at end, see Mochi.Util.Score
         */
        constructor(name, restartState, keys, score) {
            super(name);
            this.restartState = restartState;
            this.keys = keys;
            if (score)
                this.score = score;
        }

        init(game) {
            this.game = game;
            for (let i = 0; i < this.keys.length; i++) {
                this.game.input.onKeyPress(this.keys[i], this.restart.bind(this));
            }
        }

        enter (game, states, lastState) {
            if (this.score)
                this.highScore = this.score.save();
        }

        restart() {
            this.game.swapState(this.restartState);
        }

        /**
         * Draws the end game screen.
         */
        draw(ctx) {
            ctx.save();
            ctx.clearRect(0, 0, this.game.getPixelWidth(), this.game.getPixelHeight());
            ctx.fillStyle = "gray";
            ctx.fillRect(0, 0, this.game.getPixelWidth(), this.game.getPixelHeight());
            // draw the score, and if they have a high score, show it.
            let restartKeys = "Press " + this.keys.join(', ') + " to restart.";
            if (this.score) {
                this.score.draw(ctx);
                if (this.highScore) {
                    this._writeText(ctx, 'Game Over! You have a high score! ' + restartKeys,
                        this.game.getPixelWidth() / 2, this.game.getPixelHeight() / 2);
                } else {
                    this._writeText(ctx, 'Game Over! No high score! ' + restartKeys,
                        this.game.getPixelWidth() / 2, this.game.getPixelHeight() / 2);
                }
            } else {
                this._writeText(ctx, 'Game Over! ' + restartKeys,
                    this.game.getPixelWidth() / 2, this.game.getPixelHeight() / 2);
            }

            ctx.restore();
        }

        _writeText(ctx, text, x, y) {
            ctx.font = '30px serif';
            ctx.fillStyle = 'black';
            ctx.textAlign="center";
            ctx.fillText(text, x, y);
        }
    }

    return {
        GameState: GameState,
        GameStateManager: GameStateManager,
        States: {
            IntroState: IntroState,
            LoadState: LoadState,
            PauseState: PauseState,
            EndState: EndState
        }
    };
});