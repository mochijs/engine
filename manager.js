define([], function() {

    /**
     * A manager can be attached to a scene. It performs processes on entities and their components.
     * The class itself is effectively just a skeleton/interface.
     */
    class Manager {
        /**
         * Subscribes this object to any events having to do with drawing and sets the scene
         * @param scene the Mochi scene to be set
         * @param events the events object to subscribe to.
         */
        init(scene, events) {
            throw "Implement init(scene, events) method.";
        }
    }
    Manager.Name = "manager";

    return Manager;
});