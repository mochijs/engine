define(['./events/eventbus'], function(EventBus) {

    /**
     * The scene maintains a set of managers that are all currently working on a group of entities.
     * It effectively manages managers. The Event system will be used to communicate and bind to managers
     * Entities -> Components, Scene -> Managers.
     * Stores managers in a list called 'managers', but removes the word 'manager' from their title
     */
    class Scene {

        constructor() {
            /** Where all the managers are stored as an object */
            this.managers = {};
            /** Event system used to describe scene events */
            this.events = new EventBus();
            /** All of the entities currently in this scene */
            this.entities = [];
        }

        /**
         * Adds a new manager to this scene.
         * @param manager The manager to add
         * @param name? optional name to be stored. By default will use class name without word manager & lowercase.
         * @return Scene this for method chaining
         */
        addManager(manager, name) {
            // if the name is not provided via param or class, default as the class name without manager included.
            if (name == null) {
                if (manager.constructor.Name == null)
                    name = this.makeDefaultManagerName(manager);
                else
                    name = manager.constructor.Name;
            }

            let old = this.managers[name];
            // add this manager to our group
            this.managers[name] = manager;
            this.events.broadcast("manager_add", {
                manager: manager,
                old: old
            }, Scene.Name);
            return this;
        }

        /**
         * Removes the manager attached to this scene.
         * @param name The name of the manager
         */
        removeManager(name) {
            if (!(name in this.managers))
                throw "Manager is not in Scene.";
            // find the component and delete it
            let old = this.managers[name];
            delete this.managers[name];
            // broadcast this component being removed.
            // TODO find way to redo bindings or remove bindings from manager
            this.events.broadcast("manager_remove", {
                name: name,
                old: old
            }, Scene.Name);
        }

        /**
         * Initializes the scene and all the managers inside of it.
         */
        init () {
            // Initialize the managers that are not yet initialized.
            for (let name in this.managers) {
                if (!this.managers[name].initialized) {
                    this.managers[name].init(this, this.events);
                    this.managers[name].initialized = true;
                    // broadcast that this manager has now been initialized.
                    this.events.broadcast("manager_init", {
                        name: name
                    }, Scene.Name);
                }
            }
        }

        /**
         * Adds an entity to this scene and all its managers.
         * @param entity The entity to be added.
         * @return Scene this for chaining
         */
        addEntity(entity) {
            // add it to our list and broadcast our new add
            this.entities.push(entity);
            this.events.broadcast("entity_add", entity, Scene.Name);
        }

        /**
         * Removes an entity from this scene and all its managers.
         * @param entity The entity to be removed.
         */
        removeEntity(entity) {
            this.entities.splice(this.entities.indexOf(entity), 1);
            this.events.broadcast("entity_remove", entity, Scene.Name);
        }

        /**
         * Updates all of the managers inside of this scene that are bound to update
         * @param ms The amount of time since last update.
         */
        update(ms) {
            this.entities.forEach(function(e) { e.update(ms) });
            this.events.broadcast("update", ms, Scene.Name);
        }

        /**
         * Draws all of the managers inside of this scene that wish to be drawn.
         * @param ctx The context to draw on.
         */
        draw(ctx) {
            this.events.broadcast("draw", ctx, Scene.Name);
        }

        getEvents() {
            return this.events;
        }

        getEntities() {
            return this.entities;
        }

        /**
         * Returns all entities that have the specified component (by name).
         * @param name The name of the components
         */
        getEntitiesWith(name) {
            let entities = [];
            for (let i = 0; i < this.entities.length; i++) {
                if (this.entities[i].has(name))
                    entities.push(this.entities[i]);
            }
            return entities;
        }

        getManagers() {
            return this.managers;
        }

        /**
         * Returns the default name if one is not provided via the user or class def
         * @param manager manager to get one for
         * @returns {string}
         */
        makeDefaultManagerName(manager) {
            return manager.constructor.name.toLowerCase().replace("manager", "");
        }
    }
    Scene.Name = "scene";

    return Scene;
});