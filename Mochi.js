/**
 * This is the main file of the engine that just wraps up the core of the engine
 * for explicit use. Allows the project's organization to be divided between internal and external use.
 */
define([
        // The core of the game engine
        './game', './game3d', './gamestate', './entity', './input', './component', './scene', './manager',
        // the custom math libraries
        './math/math',
        // the content pipeline
        './content/asset',
        // All of the physics components
        './physics/body', './physics/shape', './physics/collisionmanager', './physics/emitter', './physics/emittermanager',
        // graphics components
        './graphics/sprites', './graphics/drawmanager', './graphics/draw3dmanager', './graphics/models',
        // the user interface components
        './graphics/ui/element', './graphics/ui/style', './graphics/ui/styles/default', './graphics/ui/elements/select', './graphics/ui/elements/text', './graphics/ui/elements/background', './graphics/ui/elements/counter',
        // the event components
        './events/eventbus',
        // artificial intelligence components
        './ai/pathfinding',
        // engine utilities and useful classes
        './util/timer', './util/unitsystem', './util/unitsystem2d', './util/score',
        // expose libraries
        './libraries/three'
    ],
function(
        // The core
        Game, Game3d, GameState, Entity, Input, Component, Scene, Manager,
        // the custom math libraries
        Math,
        // the content pipeline
        Asset,
        // physics components
        Body, Shape, CollisionManager, Emitter, EmitterManager,
        // graphics components
        Sprites, DrawManager, Draw3dManager, Models,
        // user interface components
        Element, Style, UIDefault, UISelect, UIText, UIBackground, UICounter,
        // event components
        EventBus,
        // artifical intelligence components
        PathFinding,
        // engine utilities and useful classes
        Timer, UnitSystem, UnitSystem2d, Score,
        // expose libraries
        Three
    ) {
    return {
        // The most essential components of the engine
        Game: Game,
        Game3d: Game3d,
        GameState: GameState.GameState,
        GameStateManager: GameState.GameStateManager,
        GameStates: GameState.States,
        Entity: Entity,
        Component: Component,
        Manager: Manager,
        Scene: Scene,
        // a special category of duplicates that allows for easy purview of all available components.
        Components: {
            Body: Body,
            Sprite: Sprites.Sprite,
            AnimatedSprite: Sprites.AnimatedSprite,
            Emitter: Emitter
        },
        // a special category of duplicates that allows for easy purview of all available systems.
        Managers: {
            DrawManager: DrawManager.DrawManager,
            UnitDrawManager: DrawManager.UnitDrawManager,
            CollisionManager: CollisionManager,
            EmitterManager: EmitterManager
        },
        Input: Input,
        Math: Math,
        Content: Asset,
        Physics: {
            Body: Body,
            Shape: Shape,
            CollisionManager: CollisionManager,
            Emitter: Emitter.Emitter,
            Properties: Emitter.Properties,
            EmitterManager: EmitterManager
        },
        Graphics: {
            Sprite: Sprites.Sprite,
            AnimatedSprite: Sprites.AnimatedSprite,
            DrawManager: DrawManager.DrawManager,
            UnitDrawManager: DrawManager.UnitDrawManager,
            Model: Models.Model,
            Draw3dManager: Draw3dManager,
            Ui: {
                Element: Element,
                Style: Style,
                Elements: {
                    Select: UISelect,
                    Text: UIText,
                    Background: UIBackground,
                    Counter: UICounter
                },
                Styles: {
                    Default: UIDefault
                }
            }
        },
        Events: {
            EventBus: EventBus
        },
        Ai: {
            PathFinding: PathFinding
        },
        Util: {
            Timer: Timer,
            UnitSystem: UnitSystem,
            UnitSystem2d: UnitSystem2d,
            Score: Score
        },
        Lib: {
            Three: Three
        }
    }
});
