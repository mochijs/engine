define(['../manager', './emitter'], function(Manager, Emitter) {

    class EmitterManager extends Manager {

        /** For when it's initialized with the scene */
        init (scene, events) {
            events.subscribe("update", this.update.bind(this));
            this.scene = scene;
        }

        update (ms) {
            let entities = this.scene.getEntitiesWith(Emitter.Emitter.Name);
            for(let i = 0; i < entities.length; i++) {
                // TODO add event system
                entities[i].emitter.update(ms, this.scene);
            }
        }

    }
    EmitterManager.Name = "emitter";

    return EmitterManager;
});