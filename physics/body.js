define(['./../component', './../math/math', './shape'], function(Component, math, shape) {
    /**
     * The body is the physical component of an entity that represents its physical location in a space.
     */
    class Body extends Component {
        /**
         * Creates a body component for an object.
         * @param shape the shape that the boy will be in.
         */
        constructor(shape) {
            super();
            this.shape = shape;
            this.speedX = 0;
            this.speedY = 0;
            this.accelX = 0;
            this.accelY = 0;
        }
        /**
         * Initializes an entity to be attached to this component.
         * @param entity the entity to be added.
         */
        init(entity) {
            this.entity = entity;
        }
        /**
         * Applies a force to this object.
         * @param x the force in the x direction.
         * @param y the force in the y direction.
         */
        force(x, y) {
            this.accelX += x;
            this.accelY += y;
        }
        /**
         * Moves an object a certain x and y.
         * @param x the x to be moved.
         * @param y the y to be moved.
         */
        move(x, y) {
            this.entity.x += x;
            this.entity.y += y;
        }
        /**
         * Updates the speed based on the acceleration, and moves the object based on the speed.
         * @param ms the amount of milliseconds that have passed since the last update.
         */
        update(ms) {
            this.speedX += this.accelX*ms;
            this.speedY += this.accelY*ms;
            this.move(this.speedX*ms, this.speedY*ms);
        }

        /**
         * Checks whether this body collides with the provided body.
         * @param body The body to check collision for.
         * @returns {*}
         */
        collision(body) {
            return this.shape.collision(this.entity.x, this.entity.y, this.entity.rotation, body.entity.x, body.entity.y, body.entity.rotation, body.shape);
        }

        /**
         * Invoked when this body collides with the provided entity.
         * @param entity The entity that collided with this entity's body.
         */
        onCollision(entity) {
            this.entity.events.broadcast("collision", entity, Body.Name);
        }
        /**
         * Returns the bounding box for the body.
         */
        getBoundingBox() {
            return this.shape.getBoundingBox(this.entity.x, this.entity.y, this.entity.rotation);
        }
    }
    /** The universal name of this component */
    Body.Name = "body";

    return Body;
});
