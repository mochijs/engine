// Export game prototype using requireJS.
define(['./input', './events/eventbus', './gamestate'], function(Input, EventBus, GameState) {
/**
 * The main game class. Used to create the canvas and run the main game loop.
 * It also keeps track of some graphics data pertinent to canvas.
 * In order to have a running game, the init, update, and draw functions must all be
 * overridden by the user.
 * @param data Game information needed, currently only width, height, and desired FPS.
 * @constructor
 */
class Game {
    constructor(data) {
        /** The width of the game's canvas */
        this.width = data.width;
        /** The height of the game's canvas */
        this.height = data.height;
        /** The desired maximum FPS of the game */
        this.fps = data.fps;
        /** If given, the game's unit system */
        if ('unitSystem' in data)
            this.unitSystem = data.unitSystem;
        /** The length of our consistent update */
        this.tick = 1000 / this.fps;
        /** The id of the requested frame for running the loop */
        this.requestId = [];
        /** The current time delta between last and current updates */
        this.dt = 0;
        /** The time in milliseconds the game was last updated */
        this.lastLoop = 0;
        /** The canvas object created in html */
        this.canvas = null;
        /** The context object used to draw onto the canvas */
        this.ctx = null;

        /** The object used to allow for user input */
        this.input = new Input.InputHandler(this);
        /** The EventBus that the game uses to pass its information */
        this.events = new EventBus();
        /** The game state manager used to determine the current game's state */
        this.states = new GameState.GameStateManager(this.events);
        if ("startState" in data)
            this.startState = data.startState;

        /** The number of updates behind the game uses to determine when to panic */
        this.UPDATES_NEEDED_PANIC = 200;
    };
    /**
     * A function to return the width of the game in pixels.
     */
    getPixelWidth() {
        if ('unitSystem' in this) {
            return this.unitSystem.unitsToPixelsX(this.width);
        }
        return this.width;
    };
    /**
     * A function to return the height of the game in pixels.
     */
    getPixelHeight() {
        if ('unitSystem' in this) {
            return this.unitSystem.unitsToPixelsY(this.height);
        }
        return this.height;
    };
    /**
     * Generates the canvas for the game, appends it to the main page.
     * Assumes 'document' is in scope.
     */
    start() {
        this.log("Game initializing!");
        this.log(this);
        this.running = false;
        this._setUpGraphics();
        // Initialize game's input
        this.input.init();
        // initialize the game's first state
        if (this.startState)
            this.states.swap(this.startState);
        this._enterState(this.states.current);
        // Begin the main game loop.
        this.running = true;
        this.events.broadcast("game_init", this);
        this._requestLoop();
    };

    _setUpGraphics() {
        this.log("Generating canvas for game.");
        this.canvas = document.createElement('canvas');
        // Set the dimensions of the newly created canvas, and get the graphics context
        this.canvas.width = this.getPixelWidth();
        this.canvas.height = this.getPixelHeight();
        this.ctx = this.canvas.getContext("2d");
        // Add the canvas to the document body and set it as the browser's focus (for input).
        document.body.appendChild(this.canvas);
        this.canvas.focus();
    }

    /**
     * Stops the current main game loop.
     */
    stop() {
        this.running = false;
        this.events.broadcast("game_stop", this);
        while (this.requestId.length > 0) {
            var id = this.requestId.pop();
            cancelAnimationFrame(id);
        }
    };
    /**
     * Used internally to request an update of the game.
     * Can easily be removed for efficiency issues later.
     * @private
     */
    _requestLoop() {
        var reqId = this.requestId.shift();
        this.requestId.push(requestAnimationFrame(this._main.bind(this)));
    };
    /**
     * The main game loop. Inspired by loop presented in class:
     * @source http://isaacsukin.com/news/2015/01/detailed-explanation-javascript-game-loops-and-timing
     * @param runningTime how much time the game has been running.
     * @private
     */
    _main(runningTime) {
        // Ensure enough time has occurred since our last update.
        if (runningTime < this.lastLoop + this.tick) {
            this._requestLoop();
            return;
        }
        // Delta is maintained between loops so our previous tick is maintained.
        this.dt += runningTime - this.lastLoop;
        this.lastLoop = runningTime;

        var updates = 0;
        // Update for as many physics ticks as we need unless we're way behind.
        while (this.dt >= this.tick) {
            this.states.current.update(this.tick);
            this.dt -= this.tick;
            updates++;
            if (updates >= this.UPDATES_NEEDED_PANIC) {
                this._panic();
                break;
            }
        }
        // If the game is updating at a smooth pace, render.
        this.states.current.draw(this.ctx);
        this._requestLoop();
    };
    /**
     * When the game is under high stress and needs to resolve how far its
     * dragged behind.
     * @private
     */
    _panic() {
        // TODO: Improve panic function.
        this.log("Panic! The game is " + (this.dt / this.tick) + " updates behind still.");
        this.events.broadcast("game_panic", {
            dt: this.dt,
            updatesBehind: (this.dt / this.tick)
        });
        this.dt = 0;
    };
    /**
     * Used instead of the console just in case this is run in different environments.
     * @param message The message to log.
     */
    log(message) {
        console.log(message);
    };
    /** Similar, but only debug information */
    debug(message) {
        console.log(message);
    };

    /**
     * Adds the given state to the game's state manager.
     * @param state The GameState to add.
     */
    addState(state) {
        this.states.add(state);
    };
    /**
     * Removes the given state (by name) from the game's state manager.
     * @param name The name of the GameState to remove.
     */
    removeState(name) {
        this.states.remove(name);
    };
    /**
     * Performs an add and then immediately a swap. Convenience function.
     * @param state The state to add and then swap to.
     */
    setState(state) {
        this.log("Setting State to " + state.name + ".");
        let old = this.states.current;
        this.states.set(state);
        this._enterState(old);
    };
    /**
     * Swaps the current state of the game to the one provided by name.
     * @param name The name of the state to swap the game to.
     */
    swapState(name) {
        this.log("Swapping State to " + name + ".");
        let old = this.states.current;
        this.states.swap(name);
        this._enterState(old);
    };
    /** Initializes the game's current state */
    _enterState(lastState) {
        this.log("Storing input bindings from previous state [" + lastState.name + "]...");
        // store the bindings currently on input into the last state
        lastState.storeBindings(this.input.getBindings());
        // and if this state has input bindings, apply them. Otherwise, reset them.
        if (this.states.current.loadBindings() != null)
            this.input.swapBindings(this.states.current.loadBindings());
        else
            this.input.resetBindings();
        // if the state has not been initialized, initialize it.
        if (!this.states.current.initialized) {
            this.log("Initializing state [" + this.states.current.name + "]...");
            this.states.current.init(this, this.states);
        }
        // always call enter after initialized and the swap.
        this.states.current.enter(this, this.states, lastState);
        // the game state is now ready to be used.
        this.log("State [" + this.states.current.name + "] is now ready.");
        this.states.ready();
    };
}

return Game;
});
