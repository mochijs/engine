define(['./events/eventbus'], function(EventBus) {
    class Entity {
        constructor(x, y, z = 0) {
            /** All entities have an id */
            this.id = Entity.NextId();
            /** The event bus that will handle loosely coupled messaging between components */
            this.events = new EventBus();
            /** The only pieces of data directly tied to entity aside from id are its position. */
            this.x = x;
            this.y = y;
            // Additional 3d support.
            this.z = z;
            this.rotation = 0;
            /** Components are stored within the object itself to allow direct indexing, e.g. entity.body */
            this.comps = {};
        }

        /**
         * Adds a component to this entity.
         * @param component The component to add, must have a (unique) name.
         */
        add (component) {
            if (component.constructor.Name == null)
                throw "Invalid component, does not have a name.";
            // add the component by its name.
            let old = this[component.constructor.Name];
            this[component.constructor.Name] = component;
            // Add name to the array that keeps track of the components.
            this.comps[component.constructor.Name] = true;
            // broadcast this new component being added.
            this.events.broadcast("component_add", {
                component: component,
                old: old
            }, Entity.Name);
        }

        /**
         * Initializes all the components attached to this entity, and the
         * entity itself. Allows components to set up their events and
         * the references to other components attached to this entity.
         *
         * Passes the object, the event bus, and all the components to the init function of the component.
         *
         * Keeps track of if the object has been initialized for the component.
         */
        init () {
            // Initialize the components that are not yet initialized.
            for (let name in this.comps) {
                if (!this[name].initialized) {
                    this[name].init(this, this.events);
                    this[name].initialized = true;
                    // broadcast that this component has now been initialized.
                    this.events.broadcast("component_init", {
                        name: name
                    }, Entity.Name);
                }
            }
        }

        /**
         * Removes the component of this entity
         * @param name The name of the component type (e.g. body)
         */
        remove (name) {
            if (!(name in this.comps))
                throw "Component is not in entity.";
            // find the component and delete it
            let old = this[name];
            delete this[name];
            delete this.comps[name];
            // broadcast this component being removed.
            this.events.broadcast("component_remove", {
                name: name,
                old: old
            }, Entity.Name);
        }

        /**
         * Tells the entity to update. Since entity has no functionality on its own, this merely
         * delegates to an event for any components that require update but do not have their own system.
         * @param ms The time passed since last update.
         */
        update (ms) {
            this.events.broadcast("update", {
                ms: ms,
                entity: this
            }, Entity.Name);
        }

        /**
         * Returns whether or not this entity has the given component.
         * @param name The name of the component to check
         * @returns {boolean} Whether or not this entity has it.
         */
        has (name) {
            return name in this;
        }

        /**
         * Returns the component if this entity has a component with that name.
         * @param name The name of the component to check for
         * @returns {*} The component with the given name on this entity
         */
        get (name) {
            return this[name];
        }

        /**
         * Returns the event bus (the internal messenger for the entity for components).
         * @returns {EventBus} The Event bus for this entity.
         */
        getEvents() {
            return this.events;
        }

        /**
         * Turns the entity, modifying its rotation.
         * @param rotation The rotation in radians
         */
        turn(rotation){
            this.rotation += rotation;
        }

        /**
         * Turns the entity, modifying its rotation.
         * @param rotation The rotation in degrees
         */
        turnDegrees(rotation) {
            this.rotation += rotation * Math.PI / 180;
        }

        /** @return int The next id for the new entity */
        static NextId() {
            Entity.CURRENT_ID++;
            return Entity.CURRENT_ID;
        }
    }
    /** (static) autoincrementing id of this entity */
    Entity.CURRENT_ID = 0;
    /** Entity also has a special name like a component for messaging */
    Entity.Name = "entity";

    return Entity;
});
