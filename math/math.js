/**
 * Checks if the given bounding box contains a given point.
 */
var containsPoint = function(px, py, x, y, width, height) {
    return px >= x && px <= (x + width) && py >= y && py <= (y + height);
};

/**
 * Simple function that determines if two rectangles intersect.
 * @param rect1 object with x, y, width, height
 * @param rect2 object with x, y, width, height
 */
var containsRect = function(rect1, rect2) {
    var dx = Math.abs(rect1.x - rect2.x) * 2;
    var dy = Math.abs(rect1.y - rect2.y) * 2;
    return (dx <= (rect1.width + rect2.width)) && (dy < (rect1.height + rect2.height));
};
/**
 * Defines a vector with a length of 2.
 * @param x the x value of the vector
 * @param y the y value of the vector
 */
var Vector2 = function(x, y) {
    this.x = x;
    this.y = y;
};
/**
 * Allows adding two vectors together
 * @param v the vector to be added to the first one
 */
Vector2.prototype.add = function(v){
    this.x += v.x;
    this.y += v.y;
};
/**
 * Changes degrees to radians
 * @param degrees the degrees to be changed
 */
var degreesToRadians = function(degrees) {
    return degrees * Math.PI / 180;
};

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

define([], function() {
    return {
        containsPoint: containsPoint,
        containsRect: containsRect,
        Vector2: Vector2,
        degreesToRadians: degreesToRadians,
        shuffleArray: shuffleArray
    };
});
