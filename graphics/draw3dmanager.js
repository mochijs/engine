define(['../manager', '../libraries/three', './models'], function(Manager, Three, Models) {

    /**
     * The system used for drawing model components using a 3d game context.
     */
    class Draw3dManager extends Manager {
        /**
         * Requires enough information about the scene to build a three.js scene and camera
         * @param object camera Requires a fov, width/height or aspect ratio, near coordinate (px), far coordinate (px)
         */
        constructor(camera) {
            super();
            // calculate aspect if provided width/height
            if ('width' in camera && 'height' in camera)
                camera.aspect = camera.width / camera.height;
            // create the perspective camera used to render the scene
            this.camera = new Three.PerspectiveCamera(camera.fov, camera.aspect, camera.near, camera.far);
            // stores all the model components for rendering
            this.scene3d = new Three.Scene();
        }

        /**
         * Subscribes this object to any events having to do with drawing and sets the scene
         * @param scene the Mochi scene to be set
         * @param events the events object to subscribe to.
         */
        init(scene, events) {
            events.subscribe("draw", this.draw.bind(this));
            events.subscribe("entity_add", this.add.bind(this));
            events.subscribe("entity_remove", this.remove.bind(this));
            this.scene = scene;
        }

        /**
         * Adds an entity to the 3d scene.
         * @param entity the entity to be added.
         */
        add(entity) {
            if (!entity.has(Models.Model.Name))
                return;
            this.scene3d.add(entity.model.getMesh());
        }
        /**
         * Removes an entity from the 3d scene.
         * @param the entity to be removed.
         */
        remove(entity) {
            if (!entity.has(Models.Model.Name))
                return;
            this.scene3d.remove(entity.model.getMesh());
        }

        /**
         * Converts a pixel x/y location into the unit coordinates used by the 3d space.
         * @param x The x coordinate in pixels
         * @param y The y coordinate in pixels
         * @return The 3d position at this location, at z=0
         */
        pixelsToUnits(x, y) {
            let vector = new Three.Vector3();
            vector.set(x, y, .5); // .5 is arbitrary for normalized device coords projection
            // do projection onto camera
            vector.unproject(this.camera);
            // normalize in relative to camera position
            let dir = vector.sub(this.camera.position).normalize();
            return this.camera.position.clone().add(dir.multiplyScalar(-this.camera.position.z / dir.z));
        }

        /**
         * Draws the collection of entities attached to the scene or passed in.
         * @param ctx The WebGL renderer needed to render the scene
         */
        draw(ctx) {
            let entities = this.scene.getEntitiesWith(Models.Model.Name);
            // tell all the models to update their mesh positions to the entity location
            for (let i = 0; i < entities.length; i++)
                entities[i].model.trackEntity();
            // render the scene with the web gl renderer
            ctx.render(this.scene3d, this.camera);
        }
    }
    Draw3dManager.Name = "draw3d";

    return Draw3dManager;
});