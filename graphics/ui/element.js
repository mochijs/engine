define(['../../events/eventbus'], function(EventBus) {

    /**
     * An element is a part of the GUI that can have a style applied to it.
     * All GUI elements can produce events for things that happen to them.
     * Almost all UI elements are driven by their event buses for interactivity.
     *
     * Elements work similar to entities, but are specifically for the GUI.
     */
    class Element {
        /**
         * Creates a new element.
         * @param style The style to apply to this element
         */
        constructor(style) {
            this.style = style;
            this.events = new EventBus();
        }

        getEvents() {
            return this.events;
        }

        /**
         * Changes the style of this element.
         */
        setStyle(style) {
            this.style = style;
            return this;
        }
        /**
         * Gets the height of the current element.
         */
        getHeight() {
            if ("height" in this.style)
                return this.style.height;
            return 0;
        }

        /** Must be fully implemented! */
        draw(ctx, x, y) {
            throw "Element must implement: draw(ctx, x, y)";
        }
    }

    return Element;
});
