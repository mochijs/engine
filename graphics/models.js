define(['../libraries/three', '../component'], function(Three, Component) {

    /**
     * The model class is a 3d representation of geometry and material used for drawing 3d objects using a
     * 3d draw manager.
     *
     * It is a component that can be added to any entity.
     */
    class Model extends Component {
        constructor(geometry, material) {
            super();
            this.geometry = geometry;
            this.material = material;
            this.mesh = new Three.Mesh(geometry, material);
        }

        init(entity){
            this.entity = entity;
        }

        trackEntity() {
            this.mesh.position.x = this.entity.x;
            this.mesh.position.y = this.entity.y;
            this.mesh.position.z = this.entity.z;
            this.mesh.rotation.x = this.entity.rotation;
        }

        getMesh() {
            return this.mesh;
        }
    }
    Model.Name = "model";

    return {
        Model: Model
    };
});