/**
 * This file is responsible for setting up the server configuration using Node's different import/export style.
 * It allows an HTTP server with sockets to be easily set up for networked games.
 */
// configure requirejs used in the rest of the project so it can be used in node
var requirejs = require('requirejs');
// tell requirejs that we are using it within node js
requirejs.config({
    nodeRequire: require,
    baseUrl: __dirname
});

// Require our entire game engine using requirejs instead of require.
let Mochi = requirejs('./Mochi');

// Add the server-only components to the engine
Mochi.Server = {
    GameServer: requirejs('./server/GameServer')
};

// add requirejs to Mochi
Mochi.require = requirejs;

// Use nodejs' module to export our engine.
module.exports = Mochi;